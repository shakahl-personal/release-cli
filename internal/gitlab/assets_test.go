package gitlab

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseAssetsLinkJSON(t *testing.T) {
	tests := []struct {
		name           string
		assetsLink     []string
		expectedErrMsg string
	}{
		{
			name:       "empty",
			assetsLink: []string{},
		},
		{
			name:       "one_complete_asset_link",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy"}`},
		},
		{
			name:       "one_asset_link_some_fields",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1"}`},
		},
		{
			name: "many_asset_links",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`,
				`{"name":"Asset2","url":"https://<domain>/some/location/2","link_type":"image","filepath":"xzy2"}`,
				`{"name":"Asset3","url":"https://<domain>/some/location/2","link_type":"package","filepath":"xzy3"}`},
		},
		{
			name:           "one_asset_wrong_json",
			assetsLink:     []string{`not_json`},
			expectedErrMsg: "1 error occurred:\n\t* invalid delimiter for asset: \"not_json\"\n\n",
		},
		{
			name: "multiple_assets_one_wrong_json",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`,
				`not_json`,
				`{"name":"Asset3","url":"https://<domain>/some/location/2","link_type":"package","filepath":"xzy3"}`},
			expectedErrMsg: "1 error occurred:\n\t* invalid delimiter for asset: \"not_json\"\n\n",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseAssets(tt.assetsLink)
			if tt.expectedErrMsg != "" {
				require.EqualError(t, err, tt.expectedErrMsg)
				return
			}

			require.NoError(t, err)
			if len(tt.assetsLink) == 0 {
				require.Nil(t, got)
				return
			}

			require.NotNil(t, got)
			require.Len(t, got.Links, len(tt.assetsLink))

			for k, link := range got.Links {
				b, err := json.Marshal(link)
				require.NoError(t, err)

				require.JSONEq(t, tt.assetsLink[k], string(b))
			}
		})
	}
}
