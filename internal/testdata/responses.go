package testdata

import (
	"io"
	"net/http"
	"strings"
)

const (
	ResponseCreateReleaseSuccess = iota
	ResponseBadRequest
	ResponseUnauthorized
	ResponseForbidden
	ResponseConflict
	ResponseInternalError
	ResponseUnexpectedError
	ResponseMilestoneNotFound
	ResponseGetReleaseSuccess
	ResponseGetReleaseSuccessWithHTML
	ResponseNotJSON
)

// Responses map of API possible responses obtained from the Releases API
var Responses = map[int]func() *http.Response{
	ResponseCreateReleaseSuccess: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       io.NopCloser(strings.NewReader(CreateReleaseSuccessResponse)),
		}
	},
	ResponseBadRequest: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusBadRequest,
			Body:       io.NopCloser(strings.NewReader(BadRequestResponse)),
		}
	},
	ResponseUnauthorized: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusUnauthorized,
			Body:       io.NopCloser(strings.NewReader(UnauthorizedResponse)),
		}
	},
	ResponseForbidden: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusForbidden,
			Body:       io.NopCloser(strings.NewReader(CreateReleaseForbiddenResponse)),
		}
	},
	ResponseConflict: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusConflict,
			Body:       io.NopCloser(strings.NewReader(CreateReleaseConflictResponse)),
		}
	},
	ResponseInternalError: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       io.NopCloser(strings.NewReader(InternalErrorResponse)),
		}
	},
	ResponseUnexpectedError: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       io.NopCloser(strings.NewReader(UnexpectedErrorResponse)),
		}
	},
	ResponseMilestoneNotFound: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusBadRequest,
			Body:       io.NopCloser(strings.NewReader(CreateReleaseMilestoneNotFound)),
		}
	},
	ResponseGetReleaseSuccess: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       io.NopCloser(strings.NewReader(GetReleaseResponseSuccess)),
		}
	},
	ResponseGetReleaseSuccessWithHTML: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       io.NopCloser(strings.NewReader(GetReleaseResponseSuccessWithHTML)),
		}
	},
	ResponseNotJSON: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       io.NopCloser(strings.NewReader("not-json")),
		}
	},
}

const BadRequestResponse = `
{
	"message":"tag_name is missing"
}
`

const UnauthorizedResponse = `
{
	"message":"401 Unauthorized"
}
`

const InternalErrorResponse = `
{
	"message":"500 Internal Server Error"
}
`

const UnexpectedErrorResponse = `
{
	"error":"Something went wrong"
}
`
